-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Jul 2024 pada 11.32
-- Versi server: 10.4.32-MariaDB
-- Versi PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_library`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_ebooks`
--

CREATE TABLE `data_ebooks` (
  `id` int(20) NOT NULL,
  `kode_ebook` varchar(15) NOT NULL,
  `gambars` varchar(255) NOT NULL,
  `juduls` varchar(255) NOT NULL,
  `penerbits` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `stoks` int(11) NOT NULL,
  `harga` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_ebooks`
--

INSERT INTO `data_ebooks` (`id`, `kode_ebook`, `gambars`, `juduls`, `penerbits`, `penulis`, `stoks`, `harga`, `created_at`, `updated_at`) VALUES
(2, 'HP-X2', 'img/e-book/novel2.jpg', 'Harry Potter', '-', 'J. K Rowling', 10, 10000.00, '2024-06-19 07:10:37', '2024-07-02 23:47:38'),
(3, 'TSC-X3', 'img/e-book/novel3.jpg', 'The Shark Caller', '-', 'Zillah Bethel', 3, 5000.00, '2024-06-19 07:19:57', '2024-06-29 08:56:41'),
(4, 'TPP-X4', 'img/e-book/novel4.jpg', 'The Paper Magician', '-', 'Charlie N. Holberg', 7, 20000.00, '2024-06-19 07:19:57', '2024-07-02 23:49:46'),
(5, 'BM-X5', 'img/e-book/novel5.jpg', 'Bumi', 'Gramedia Pustaka Utama', 'Tere Liye', 15, 2000.00, '2024-06-19 07:23:22', '2024-06-19 07:23:22'),
(6, 'TPW-X6', 'img/e-book/novel6.jpg', 'The Poppy War', '-', 'R. F. Kuang', 20, 3000.00, '2024-06-19 07:23:22', '2024-06-19 07:23:22'),
(7, 'PC-X7', 'img/e-book/novel7.jpg', 'Percy Jakcson', '-', 'Rick Riordon', 15, 5000.00, '2024-06-19 07:26:08', '2024-06-19 07:26:08'),
(8, 'THO-X8', 'img/e-book/novel8.jpg', 'The Heroes Of Olympus', '-', 'Rick Riordon', 5, 4000.00, '2024-06-19 07:26:08', '2024-07-02 23:51:47'),
(9, 'TRA-X9', 'img/e-book/novel9.jpg', 'The Trials Of Apollo', '-', 'Rick Riordon ', 3, 6000.00, '2024-06-19 07:30:42', '2024-06-19 07:30:42'),
(10, 'TBG-X10', 'img/e-book/novel10.jpg', 'The Burning God', '-', 'R. F. Kuang', 1, 3000.00, '2024-06-19 07:30:42', '2024-07-02 23:47:44'),
(11, 'HBJ-X11', 'img/e-book/novellromance10.jpg', 'Hujan Bulan Juni', '-', 'Safardi Djoko Damono', 5, 5000.00, '2024-06-20 14:22:54', '2024-06-20 14:22:54'),
(12, 'D-X12', 'img/e-book/novelromance1.jpg', 'Dilan 1990', '-', 'Pidi Baiq', 100, 2000.00, '2024-06-20 14:33:11', '2024-06-20 14:33:11'),
(14, 'MSD-X14', 'img/e-book/novelromance3.jpg', 'Milea Suara Dilan', '-', 'Pidi Baiq', 100, 2000.00, '2024-06-20 14:36:09', '2024-06-20 14:36:09'),
(15, 'ANC-X15', 'img/e-book/novelromance4.jpg', 'Ancika 1995', '-', 'Pidi Baiq', 100, 1000.00, '2024-06-20 14:36:09', '2024-06-20 14:36:09'),
(16, 'RJ-X16', 'img/e-book/novelromance6.jpg', 'Romeo & Juliet', '-', '-', 50, 2000.00, '2024-06-20 14:41:08', '2024-06-20 14:41:08'),
(17, 'DN-X17', 'img/e-book/novelromance7.jpg', 'Dear Nathan', '-', 'Erisca Febriani', 40, 4000.00, '2024-06-20 14:41:08', '2024-06-20 14:41:08'),
(18, 'CUS-X18', 'img/e-book/novelromance8.jpg', 'Cinta Untuk Starla', '-', 'Tisa TS', 30, 5000.00, '2024-06-20 14:44:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_librarys`
--

CREATE TABLE `data_librarys` (
  `id` int(20) NOT NULL,
  `kode_buku` varchar(15) NOT NULL,
  `gambars` varchar(255) NOT NULL,
  `juduls` varchar(255) NOT NULL,
  `penerbits` varchar(255) NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `stoks` int(11) NOT NULL,
  `raks` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_librarys`
--

INSERT INTO `data_librarys` (`id`, `kode_buku`, `gambars`, `juduls`, `penerbits`, `penulis`, `stoks`, `raks`, `created_at`, `updated_at`) VALUES
(1, 'SMW-CA', 'img/e-library/gambar-1.jpg', 'Seni Manajemen Waktu', 'Cecilia', 'Cecilia', 14, '3A', '2024-06-19 04:29:07', '2024-06-29 23:24:11'),
(2, 'YBNS-3A', 'img/e-library/gambar-2.jpg', 'Yuk Belajar Nabung Saham', 'Ryan', 'Ryan', 19, '3A', '2024-06-19 04:29:07', '2024-06-30 00:41:13'),
(3, 'DPJ-3B', 'img/e-library/gambar-3.jpg', 'Dasar Pemograman Java', 'Yogyakarta : Andi, 2005', 'Abdul Kadir', 4, '3B', '2024-06-19 04:32:01', '2024-06-30 06:33:48'),
(4, 'BP-3B', 'img/e-library/gambar-4.jpg', 'Business Plan', 'Yudi', 'Yudi', 23, '3B', '2024-06-19 04:32:01', '2024-07-02 23:51:56'),
(5, 'TPOM-3B', 'img/e-library/gambar-5.jpg', 'The Psychology of Money', 'Morgan Housel', 'Morgan Housel', 27, '3B', '2024-06-19 04:34:41', '2024-06-30 06:33:54'),
(6, 'BSH-3C', 'img/e-library/gambar-6.jpg', 'Buku Sakti Hacker', 'Mediakita', 'Efvy Zam', 31, '3C', '2024-06-19 04:34:41', '2024-07-02 23:49:57'),
(7, 'BCR-3C', 'img/e-library/bukublj.jpg', 'Belajar Coding itu Penting di Era Revolusi 4.0', 'Pustaka Baru Press', 'Yeni Mulyani\r\n', 50, '3C', '2024-06-21 07:00:25', '2024-06-21 07:00:25'),
(8, 'TPP-3C', 'img/e-library/bukublj1.jpg', 'The Progmatic Programmer', 'Addison-Wesley', 'Andrew berburu & David Thomas', 5, '3C', '2024-06-21 07:00:25', '2024-06-21 07:00:25'),
(9, 'BBJ-3C', 'img/e-library/bukublj3.jpg', 'Buku Pertama Belajar Pemrograman Java', 'Buku Seru', 'Abdul Kadir\r\n', 13, '3C', '2024-06-21 07:09:55', '2024-06-21 07:09:55'),
(10, 'PWD-3A', 'img/e-library/bukublj4.jpg', 'Pemrograman Web Dasar', 'Yogyakarta: Deepublish, 2018.', 'Rintho Rante Rerung ', 14, '3A', '2024-06-14 07:09:55', '2024-06-14 07:09:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pinjamans`
--

CREATE TABLE `data_pinjamans` (
  `id` int(20) NOT NULL,
  `id_elibrary` int(20) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `kode_buku` varchar(20) NOT NULL,
  `tgl_meminjam` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tgl_pengembalian` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `denda` decimal(8,2) NOT NULL DEFAULT 0.00,
  `status_denda` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `data_pinjamans`
--

INSERT INTO `data_pinjamans` (`id`, `id_elibrary`, `nama_user`, `kode_buku`, `tgl_meminjam`, `tgl_pengembalian`, `denda`, `status_denda`, `created_at`, `updated_at`) VALUES
(36, 1, 'Rizza Widi Nugraha', 'DPJ-3B', '2024-06-18 13:42:58', '2024-06-28 13:42:58', 10000.00, '0', '2024-06-18 06:33:48', '2024-06-18 06:33:48'),
(37, 1, 'Rizza Widi Nugraha', 'TPOM-3B', '2024-06-30 13:33:54', '2024-06-30 13:33:54', 0.00, '0', '2024-06-30 06:33:54', '2024-06-30 06:33:54'),
(38, 1, 'Rizza Widi Nugraha', 'BSH-3C', '2024-06-30 13:47:01', '2024-06-30 13:47:01', 0.00, '1', '2024-06-30 06:33:57', '2024-06-30 06:33:57'),
(39, 1, 'Rizza Widi Nugraha', 'BSH-3C', '2024-07-03 06:49:57', '2024-07-03 06:49:57', 0.00, '0', '2024-07-02 23:49:57', '2024-07-02 23:49:57'),
(40, 1, 'Rizza Widi Nugraha', 'BP-3B', '2024-07-03 06:51:56', '2024-07-03 06:51:56', 0.00, '0', '2024-07-02 23:51:56', '2024-07-02 23:51:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `genres`
--

CREATE TABLE `genres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gambars` varchar(255) NOT NULL,
  `juduls` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rizza Widi Nugraha', 'rizzawidinugraha@gmail.com', '2024-06-19 13:02:32', '$2a$12$hDWmKbGTzJMRQVM5NLbXLemrgh5REra4nj1S9RxPUKQQtER7UqDii', '12345', '2024-06-19 13:02:32', '2024-06-19 13:02:32'),
(7, 'Dede', 'dede12@gmail.com', '2024-06-21 07:53:02', '$2y$10$8d9Go2OkdIawGn8RAIEUO.isDufPR556HbfTqhQSUxIN4qL8S7S/y', 'PxcZjUIfya', '2024-06-21 07:53:02', '2024-06-21 07:53:02'),
(8, 'Ari Rahman', 'arirahman@gmail.com', '2024-06-21 08:00:14', '$2y$10$VxKe5xBB/iqC/C16kleiFemAm93arMMAmDRXonLeFuoOzrGxxTyBa', 'vIk1w0UypC', '2024-06-21 08:00:14', '2024-06-21 08:00:14'),
(9, 'faisyal', 'faisyal12@gmail.com', '2024-06-21 08:07:53', '$2y$10$OprGsnthc8H6H26fV6Np4OHt1FjNpIOP5BjFMtjOBo6VOqgH8CRJm', '37H95GCQ6s', '2024-06-21 08:07:53', '2024-06-21 08:07:53'),
(10, 'Icang', 'icang123@gmail.com', '2024-06-21 08:27:08', '$2y$10$e2DFCZ8HJBpCsrdovtp9X.nAeqAfRaJCPUvwJQIQj67KTaeODXaLq', 'TeZVJJwxPY', '2024-06-21 08:27:08', '2024-06-21 08:27:08'),
(11, 'Nandang Kosasih', 'nandang@gmail.com', '2024-06-23 19:06:39', '$2y$10$QNNyGDx2L0SIJQfkDLmUn.iv5fEeRFg.cX8p7Rv/mZZvwFx6TxpXS', 'WotRkY3wu7', '2024-06-23 19:06:39', '2024-06-23 19:06:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(15, '2024_06_19_041325_data_ebooks', 1),
(16, '2024_06_19_042114_data_elibrarys', 1),
(17, '2024_06_19_062128_genres', 1),
(18, '2024_06_19_080628_members', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelians`
--

CREATE TABLE `pembelians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `kode_buku` varchar(255) NOT NULL,
  `ebook_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_pembelian` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `harga` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pembelians`
--

INSERT INTO `pembelians` (`id`, `user_id`, `nama_user`, `kode_buku`, `ebook_id`, `tanggal_pembelian`, `harga`, `created_at`, `updated_at`) VALUES
(14, 1, 'Rizza Widi Nugraha', 'HP-X2', 2, '2024-07-02 23:47:38', 10000.00, '2024-07-02 23:47:38', '2024-07-02 23:47:38'),
(15, 1, 'Rizza Widi Nugraha', 'TBG-X10', 10, '2024-07-02 23:47:44', 3000.00, '2024-07-02 23:47:44', '2024-07-02 23:47:44'),
(16, 1, 'Rizza Widi Nugraha', 'TPP-X4', 4, '2024-07-02 23:49:46', 20000.00, '2024-07-02 23:49:46', '2024-07-02 23:49:46'),
(17, 1, 'Rizza Widi Nugraha', 'THO-X8', 8, '2024-07-02 23:51:47', 4000.00, '2024-07-02 23:51:47', '2024-07-02 23:51:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Muhammad Faisyal', 'faisyalnur04@gmail.com', '2024-06-19 13:04:18', '$2a$12$qJWEb161df8/4MhrfjtIGODqxGdY9erOlOSej/LCQvN9qwjnl7.RG', '67890', '2024-06-19 13:04:18', '2024-06-19 13:04:18');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_ebooks`
--
ALTER TABLE `data_ebooks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_librarys`
--
ALTER TABLE `data_librarys`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_pinjamans`
--
ALTER TABLE `data_pinjamans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_elibrary` (`id_elibrary`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_email_unique` (`email`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indeks untuk tabel `pembelians`
--
ALTER TABLE `pembelians`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembelians_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_ebooks`
--
ALTER TABLE `data_ebooks`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `data_librarys`
--
ALTER TABLE `data_librarys`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `data_pinjamans`
--
ALTER TABLE `data_pinjamans`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `genres`
--
ALTER TABLE `genres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `pembelians`
--
ALTER TABLE `pembelians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `data_pinjamans`
--
ALTER TABLE `data_pinjamans`
  ADD CONSTRAINT `data_pinjamans_ibfk_1` FOREIGN KEY (`id_elibrary`) REFERENCES `data_librarys` (`id`);

--
-- Ketidakleluasaan untuk tabel `pembelians`
--
ALTER TABLE `pembelians`
  ADD CONSTRAINT `pembelians_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
