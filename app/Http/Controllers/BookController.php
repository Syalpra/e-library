<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();
        return view('admin.data-ebook', ['ebookList' => $books]);
    }

    public function create()
    {
        return view('admin.tambah-data-ebook');
    }

    public function store(Request $request)
    {
        $request->validate([
            'kode_ebook' => 'required',
            'gambars' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'juduls' => 'required',
            'penulis' => 'required',
            'penerbits' => 'required',
            'stoks' => 'required|integer',
            'harga' => 'required|numeric',
        ]);

        // Simpan gambar
        $imagePath = $request->file('gambars')->store('img/e-book', 'public');

        $book = new Book();
        $book->kode_ebook = $request->kode_ebook;
        $book->gambars = $imagePath;
        $book->juduls = $request->juduls;
        $book->penulis = $request->penulis;
        $book->penerbits = $request->penerbits;
        $book->stoks = $request->stoks;
        $book->harga = $request->harga;
        $book->save();

        return redirect()->route('admin.data-ebook')->with('success', 'Data e-book berhasil ditambahkan');
    }

    public function edit($id)
    {
        $book = Book::findOrFail($id);
        return view('admin.edit-data-ebook', compact('book'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_ebook' => 'required',
            'gambars' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'juduls' => 'required',
            'penulis' => 'required',
            'penerbits' => 'required',
            'stoks' => 'required|integer',
            'harga' => 'required|numeric',
        ]);

        $book = Book::findOrFail($id);

        $imagePath = $request->file('gambars')->store('img/e-book', 'public');

        $book->kode_ebook = $request->kode_ebook;
        $book->gambars = $imagePath;
        $book->juduls = $request->juduls;
        $book->penulis = $request->penulis;
        $book->penerbits = $request->penerbits;
        $book->stoks = $request->stoks;
        $book->harga = $request->harga;
        $book->save();

        return redirect()->route('admin.data-ebook')->with('success', 'Data e-book berhasil diperbarui');
    }

    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        Storage::delete('public/' . $book->gambars);
        $book->delete();

        return redirect()->route('admin.data-ebook')->with('success', 'E-book berhasil dihapus');
    }
}
