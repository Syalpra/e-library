<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pembelian;

class PembelianController extends Controller
{
    public function index()
    {
        $pembelianList = Pembelian::all();
        return view('member.pembelian', ['pembelianList' => $pembelianList]);

    }

    public function index2() {
        
        $pembelianList = Pembelian::all();
        return view('admin.data-pembelian', ['pembelianList' => $pembelianList]);
    }

    
}
