<?php

namespace App\Http\Controllers;

use App\Models\DataMember;
use Illuminate\Http\Request;

class DataMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $member = DataMember::all();
        return view('admin.dashboard', compact('member'));
    }

    public function index2()
    {
        $member = DataMember::all();
        return view('admin.data-users', compact('member'));
    }
}
