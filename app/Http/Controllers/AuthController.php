<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Member;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        // Validasi input dari form
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Mengambil input dari form
        $credentials = $request->only('email', 'password');

        // Cek kredensial di tabel users (admins)
        if (Auth::guard('admin')->attempt($credentials)) {
            // Autentikasi berhasil untuk admin, set user session
            $user = Auth::guard('admin')->user();
            Session::put('user', [
                'email' => $user->email,
                'nama' => $user->name,
            ]);

            return redirect()->intended('admin');
        }

        // Cek kredensial di tabel members
        if (Auth::guard('member')->attempt($credentials)) {
            // Autentikasi berhasil untuk member, set user session
            $user = Auth::guard('member')->user();
            Session::put('user', [
                'email' => $user->email,
                'nama' => $user->name,
            ]);

            return redirect()->intended('member');
        }

        // Autentikasi gagal, kembali ke form login dengan pesan error
        return back()->withErrors([
            'login' => 'Username atau Password yang anda input salah',
        ]);
    }

    public function logout()
    {
        // Logout user dan hapus session
        Auth::logout();
        Session::forget('user');
        return redirect()->route('login');
    }
}
