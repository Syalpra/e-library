<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BookMember;
use App\Models\Pembelian;

class BookMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    public function index()
    {
        $book = BookMember::all();
        return view('member.ebook', ['ebookList' => $book]);
    }

    public function beliEbook($id)
    {
    if (auth()->guard('member')->check()) {
        $member = auth()->guard('member')->user();

        $ebook = BookMember::findOrFail($id);

        // Simpan data pembelian
      
        Pembelian::create([
            'user_id' => $member->id,
            'nama_user' => $member->name,
            'kode_buku' => $ebook->kode_ebook,
            'ebook_id' => $ebook->id,
            'harga' => $ebook->harga,
            'tanggal_pembelian' => now(),
        ]);

        // Kurangi stok e-book
        $ebook->stoks -= 1;
        $ebook->save();

        return redirect()->back()->with('success', 'E-book berhasil dibeli!');
    } else {
        return redirect()->route('login')->with('error', 'Anda harus login terlebih dahulu untuk membeli e-book.');
    }
}

}
