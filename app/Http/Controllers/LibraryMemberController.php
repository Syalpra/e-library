<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LibraryMember;
use App\Models\DataPinjaman;
use Carbon\Carbon;

class LibraryMemberController extends Controller
{
    public function index()
    {
        $library = LibraryMember::all();
        return view('member.elibrary', ['elibraryList' => $library]);
    }

    public function pinjamBuku(Request $request, $id)
    {
        if (auth()->guard('member')->check()) {
            $member = auth()->guard('member')->user();
            $library = LibraryMember::findOrFail($id);

            DataPinjaman::create([
                'id_elibrary' => $member->id,
                'nama_user' => $member->name,
                'kode_buku' => $library->kode_buku,
                'tanggal_meminjam' => Carbon::now(),
                'tanggal_pengembalian' => Carbon::now()->addDays(7),
                'status_denda' => 0,
            ]);

            // Kurangi stok e-book
            $library->stoks -= 1;
            $library->save();

            return redirect()->back()->with('success', 'Buku berhasil dipinjam');
        }

        return redirect()->back()->with('error', 'Anda harus login sebagai member untuk meminjam buku');
    }
}
