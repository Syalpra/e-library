<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Library;
use Illuminate\Support\Facades\Storage;

class LibraryController extends Controller
{
    public function index()
    {
        $library = Library::all();
        return view('admin.data-elibrary', ['elibraryList' => $library]);
    }

    
    public function create()
    {
        return view('admin.tambah-data-elibrary');
    }

    public function store(Request $request)
    {
        $request->validate([
            'kode_buku' => 'required',
            'gambars' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'juduls' => 'required',
            'penulis' => 'required',
            'penerbits' => 'required',
            'stoks' => 'required|integer',
            'raks' => 'required|integer',
        ]);

        // Simpan gambar
        $imagePath = $request->file('gambars')->store('img/e-library', 'public');

        $library = new Library();
        $library->kode_buku = $request->kode_buku;
        $library->gambars = $imagePath;
        $library->juduls = $request->juduls;
        $library->penulis = $request->penulis;
        $library->penerbits = $request->penerbits;
        $library->stoks = $request->stoks;
        $library->raks = $request->raks;
        $library->save();

        return redirect()->route('admin.data-elibrary')->with('success', 'Data e-Library berhasil ditambahkan');
    }

    public function edit($id)
    {
        $library = Library::findOrFail($id);
        return view('admin.edit-data-elibrary', compact('library'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_buku' => 'required',
            'gambars' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'juduls' => 'required',
            'penulis' => 'required',
            'penerbits' => 'required',
            'stoks' => 'required|integer',
            'raks' => 'required|integer',
        ]);

        $library = Library::all();

        $imagePath = $request->file('gambars')->store('img/e-library', 'public');

        $library = new Library();
        $library->kode_buku = $request->kode_buku;
        $library->gambars = $imagePath;
        $library->juduls = $request->juduls;
        $library->penulis = $request->penulis;
        $library->penerbits = $request->penerbits;
        $library->stoks = $request->stoks;
        $library->raks = $request->raks;

        $library->save();

        return redirect()->route('admin.data-elibrary')->with('success', 'Data e-Library berhasil diperbarui');
    }

    public function destroy($id)
    {
        $library = Library::findOrFail($id);
        Storage::delete('public/' . $library->gambars);
        $library->delete();

        return redirect()->route('admin.data-elibrary')->with('success', 'E-Library berhasil dihapus');
    }
}
