<?php
namespace App\Http\Controllers;

use App\Models\DataPinjaman;
use Illuminate\Http\Request;

class DataPinjamanController extends Controller
{
    public function index()
    {
        $pinjamanList = DataPinjaman::all(); // Ambil semua data pinjaman
        return view('member.data-pinjaman', ['pinjamanList' => $pinjamanList]);
    }
    public function index2()
    {
        $pinjamanList = DataPinjaman::all(); // Ambil semua data pinjaman
        return view('admin.data-pinjaman', ['pinjamanList' => $pinjamanList]);
    }

    // Metode untuk bayar denda (simulasi)
    public function bayarDenda($id)
    {
        $pinjaman = DataPinjaman::find($id);
        if (!$pinjaman) {
            return redirect()->back()->with('error', 'Data Pinjaman tidak ditemukan');
        }

        // Cek apakah sudah ada pembayaran denda
        if ($pinjaman->denda <= 0) {
            return redirect()->back()->with('error', 'Denda sudah dibayar sebelumnya');
        }

        // Simulasi pembayaran sukses
        $pinjaman->denda;
        $pinjaman->status_denda = true;
        $pinjaman->save();

        return redirect()->back()->with('success', 'Denda berhasil dibayar | Harap kembalikan buku tepat waktu agar tidak terkena denda');
    }

    // Metode untuk menandai buku sudah dikembalikan jika denda sudah terbayar
    public function bukuDikembalikan($id)
    {
        $pinjaman = DataPinjaman::find($id);
        if (!$pinjaman) {
            return redirect()->back()->with('error', 'Data Pinjaman tidak ditemukan');
        }

        // Logika untuk menandai buku sudah dikembalikan
        // Misalnya:
        $pinjaman->status_denda = true;
        $pinjaman->save();

        return redirect()->back()->with('success', 'Status pengembalian buku berhasil diperbarui');
    }
    
    
}
