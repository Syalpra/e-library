<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{

    protected $table = 'members';  // Menggunakan tabel 'members' untuk member
    
    protected $fillable = [
        'email', 'password', 'name',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}



