<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'users';  // Menggunakan tabel 'users' untuk admin
    
    protected $fillable = [
        'email', 'password', 'name',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}


