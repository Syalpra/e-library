<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    use HasFactory;
    protected $table = 'data_librarys';

    protected $fillable = [
        'kode_buku',
        'juduls',
        'penerbits',
        'penulis',
        'stoks',
        'gambars',
        'raks' 
    ];
    
}
