<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = 'data_ebooks'; // Sesuaikan dengan nama tabel yang benar


    protected $fillable = [
        'kode_ebook',
        'juduls',
        'penerbits',
        'penulis',
        'stoks',
        'gambars',
        'harga',
        'raks' 
    ];
}
