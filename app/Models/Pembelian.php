<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'nama_user',
        'kode_buku',
        'ebook_id',
        'harga',
        'tanggal_pembelian',
    ];

    public function ebook()
    {
        return $this->belongsTo(BookMember::class, 'ebook_id');
    }
}

