<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class DataPinjaman extends Model
{
    protected $table = 'data_pinjamans'; // Sesuaikan dengan nama tabel yang benar
    

    protected $fillable = [
        'id_elibrary',
        'nama_user',
        'kode_buku',
        'tgl_meminjam',
        'tgl_pengembalian',
        'denda',
        'status_denda'
    ];

    public function calculateFine()
    {
        if ($this->tgl_meminjam && !$this->tgl_pengembalian) {
            $dueDate = Carbon::parse($this->tgl_meminjam)->addWeek();
            $currentDate = Carbon::now();
            if ($currentDate->greaterThan($dueDate)) {
                $daysOverdue = $currentDate->diffInDays($dueDate);
                $finePerDay = 1000; // Misalnya denda per hari adalah 1000
                $this->denda = $daysOverdue * $finePerDay;
                $this->save();
            }
        }
    }

    public static function getTotalDenda()
    {
        return self::sum('denda');
    }
    
}

