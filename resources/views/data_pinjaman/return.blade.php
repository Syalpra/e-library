<!-- resources/views/data_pinjaman/return.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <title>Return Book</title>
</head>
<body>
    <h1>Return Book</h1>

    @if(session('error'))
        <p style="color: red;">{{ session('error') }}</p>
    @endif

    <form action="{{ route('return-book.submit', $dataPinjaman->id) }}" method="POST">
        @csrf
        <p>Are you sure you want to return the book with ID {{ $dataPinjaman->data_ebook_id }}?</p>
        <button type="submit">Return Book</button>
    </form>
</body>
</html>
