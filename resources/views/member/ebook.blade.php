@extends('sidebar.membersidebar')

@section('Judul', 'E-Library | Data E-Book')

@section('SubJudul', 'Data E-book')

@section('isi-konten')
      <div class="content">
        <div class="container-fluid">
          @if(session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          @if(session('error'))
              <div class="alert alert-danger">
                  {{ session('error') }}
              </div>
          @endif
          
            <div class="row">
                <div class="col-lg-12 col-md-12">
                  <div class="card">
                    <div class="card-header card-header-primary">
                      <h4 class="card-title">Data E-book</h4>
                    </div>
                    <div class="card-body table-responsive">
                      <table class="table table-hover">
                        <thead class="text-warning">
                          <th>ID</th>
                          <th>Kode Ebook</th>
                          <th>Gambar</th>
                          <th>Judul</th>
                          <th>Penulis</th>
                          <th>Penerbit</th>
                          <th>Harga</th>
                          <th>Stok</th>
                          <th>Aksi</th>
                        </thead>
                        <tbody>
                        @foreach ($ebookList as $data)
                          <tr>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->kode_ebook }}</td>
                            <td><img src="../{{ $data->gambars }}" style="height: 100px;"></td>
                            <td>{{ $data->juduls }}</td>
                            <td>{{ $data->penulis }}</td>
                            <td>{{ $data->penerbits }}</td>
                            <td>{{ $data->harga }}</td>
                            <td>{{ $data->stoks }}</td>
                            <td>
                                <form action="{{ route('member.beli-ebook', $data->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-success">Beli</button>
                                </form>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        </div>
    </div>

@endSection
