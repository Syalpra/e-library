@extends('sidebar.membersidebar')

@section('Judul', 'E-Library | Dashboard')

@section('SubJudul', 'Dashboard')

@section('isi-konten')
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-success">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <h3 class="card-title">Selamat Datang di Aplikasi E-Library</h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <ul>
                    <li>Disini kamu bisa meminjam buku secara online</li>
                    <li>Kamu juga bisa membeli E-book berbentuk pdf</li>
                    <li>Pembayaran denda dan pembelian buku bisa menggunakan e-money</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endSection