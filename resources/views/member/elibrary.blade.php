@extends('sidebar.membersidebar')

@section('Judul', 'E-Library | Data E-Book')

@section('SubJudul', 'Data E-Library')

@section('isi-konten')
<div class="content">
    <div class="container-fluid">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Data E-Library</h4>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <th>ID</th>
                                <th>Kode Buku</th>
                                <th>Gambar</th>
                                <th>Judul</th>
                                <th>Penulis</th>
                                <th>Penerbit</th>
                                <th>Stok</th>
                                <th>Rak</th>
                                <th>Tindakan</th>
                            </thead>
                            <tbody>
                            @foreach ($elibraryList as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->kode_buku }}</td>
                                    <td><img src="../{{ $data->gambars }}" style="height: 100px;"></td>
                                    <td>{{ $data->juduls }}</td>
                                    <td>{{ $data->penulis }}</td>
                                    <td>{{ $data->penerbits }}</td>
                                    <td>{{ $data->stoks }}</td>
                                    <td>{{ $data->raks }}</td>
                                    <td>
                                        <form action="{{ route('pinjam.buku', ['id' => $data->id]) }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="id_elibrary" value="{{ $data->id }}">
                                            <button type="submit" class="btn btn-success btn-sm">Pinjam Buku</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
