<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | E-Library</title>
    <link rel="apple-touch-icon" sizes="76x76" href="../img/logo-2.png">
    <link rel="icon" type="image/png" href="../img/logo-2.png">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: #212529;
            color: #ffffff;
        }
        .login-container {
            margin-top: 100px;
        }
        .login-card {
            background-color: #343a40;
            border: 1px solid #495057;
        }
        .login-card .form-control {
            background-color: #495057;
            color: #ffffff;
        }
        .login-card .btn-primary {
            background-color: #0d6efd;
            border-color: #0d6efd;
        }
        .profile-img {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100px;
            height: 100px;
            border-radius: 50%;
            border: 3px solid #0d6efd;
            margin-bottom: 20px;
        }
        .no-underline {
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container login-container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="card login-card">
                    <div class="card-body text-center">
                        <img src="img/logo-2.png" alt="Profile Image" class="profile-img">
                        <h3 class="card-title mb-4">Login | E-Library</h3>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <input type="email" class="form-control" id="email" name="email" placeholder="email">
                            </div>
                            <div class="mb-3">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="d-grid">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                            <div class="mb-3 mt-3">
                                <p>Belum punya akun? <a href="/register" class="no-underline">Daftar disini</a></p>
                            </div>
    
                        @if($errors->any())
                            <div class="alert alert-danger mt-4">
                                @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
</body>
</html>
