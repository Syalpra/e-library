@extends('sidebar.adminsidebar')

@section('Judul', 'E-Library | Data Users')

@section('SubJudul', 'Data Users')

@section('isi-konten')
      <div class="content">
        <div class="container-fluid">
          
            <div class="row">
                <div class="col-lg-12 col-md-12">
                  <div class="card">
                    <div class="card-header card-header-primary">
                      <h4 class="card-title">Data Users</h4>
                    </div>
                    <div class="card-body table-responsive">
                      <table class="table table-hover">
                        <thead class="text-warning">
                          <th>ID</th>
                          <th>Nama</th>
                          <th>Email</th>
                          <th>Password</th>
                          <th>Terdaftar</th>
                          <th>Aksi</th>
                        </thead>
                        <tbody>
                        @foreach ($member as $data)
                          <tr>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->email }}</td>
                            <td>{{ $data->password }}</td>
                            <td>{{ $data->created_at }}</td>
                            <td>
                                <a href="/admin/edit-data-ebook/{{ $data->id }}" class="btn btn-warning btn-sm">Edit</a>
                                <form action="/admin/hapus-data-ebook/{{ $data->id }}" method="POST" style="display:inline-block;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                </form>
                            </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
        </div>
    </div>

@endSection