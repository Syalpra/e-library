@extends('sidebar.adminsidebar')

@section('Judul', 'E-Library | Data E-library')

@section('SubJudul', 'Data E-library')

@section('isi-konten')
    <div class="content">
        <div class="container-fluid">
          @if(session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary d-flex justify-content-between align-items-center">
                            <h4 class="card-title m-0">Data E-library</h4>
                            <button class="btn btn-success text-dark"><a href="/admin/tambah-data-elibrary">Tambah Data</a></button>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover">
                                <thead class="text-warning">
                                    <tr>
                                        <th>ID</th>
                                        <th>Gambar</th>
                                        <th>Judul</th>
                                        <th>Penulis</th>
                                        <th>Penerbit</th>
                                        <th>Stok</th>
                                        <th>Rak</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($elibraryList as $data)
                                        <tr>
                                            <td>{{ $data->id }}</td>
                                            <td><img src="../{{ $data->gambars }}" style="height: 100px;"></td>
                                            <td>{{ $data->juduls }}</td>
                                            <td>{{ $data->penulis }}</td>
                                            <td>{{ $data->penerbits }}</td>
                                            <td>{{ $data->stoks }}</td>
                                            <td>{{ $data->raks }}</td>
                                            <td>
                                                <a href="/admin/edit-data-elibrary/{{ $data->id }}" class="btn btn-warning btn-sm">Edit</a>
                                                <form action="/admin/hapus-data-library/{{ $data->id }}" method="POST" style="display:inline-block;">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
