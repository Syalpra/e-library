@extends('sidebar.adminsidebar')

@section('Judul', 'E-Library | Data Pinjaman')

@section('SubJudul', 'Data Pinjaman')

@section('isi-konten')
<div class="content">
    <div class="container-fluid">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary d-flex justify-content-between align-items-center">
                        <h4 class="card-title m-0">Data Pinjaman</h4>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <tr>
                                    <th>ID</th>
                                    <th>Kode Buku</th>
                                    <th>Peminjam</th>
                                    <th>Tanggal Meminjam</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Denda</th>
                                    <th>Status</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pinjamanList as $pinjaman)
                                    <tr>
                                        <td>{{ $pinjaman->id }}</td>
                                        <td>{{ $pinjaman->kode_buku }}</td>
                                        <td>{{ $pinjaman->nama_user }}</td>
                                        <td>{{ $pinjaman->tgl_meminjam }}</td>
                                        <td>{{ $pinjaman->tgl_pengembalian }}</td>
                                        <td>{{ $pinjaman->denda }}</td>
                                        <td>
                                            @if ($pinjaman->tgl_pengembalian == null)
                                                Masa Peminjaman
                                            @elseif ($pinjaman->status_denda == 1)
                                                Buku Telah Dikembalikan
                                            @else
                                                Harap Segera Kembalikan
                                            @endif
                                        </td>
                                        <td>
                                            @if ($pinjaman->tgl_pengembalian == null)
                                                {{-- Status: Masa Peminjaman --}}
                                                {{-- Tidak ada tindakan --}}
                                            @elseif ($pinjaman->status_denda == 1)
                                                {{-- Status: Buku Telah Dikembalikan --}}
                                                {{-- Tidak ada tindakan --}}
                                            @else
                                                {{-- Status: Masa Peminjaman --}}
                                                {{-- Tidak ada tindakan --}}
                                                {{-- Status: Harap Segera Kembalikan --}}
                                                @if ($pinjaman->denda == 0)
                                                    <form action="{{ route('member.buku-dikembalikan', $pinjaman->id) }}" method="POST" style="display:inline-block;">
                                                        @csrf
                                                        <button type="submit" class="btn btn-primary btn-sm">Kembalikan Buku</button>
                                                    </form>
                                                @else
                                                    <form action="{{ route('member.bayar-denda', $pinjaman->id) }}" method="POST">
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger btn-sm">Hub. Peminjam</button>
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
