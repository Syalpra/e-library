@extends('sidebar.adminsidebar')

@section('Judul', 'E-Library | Data Pembelian')

@section('SubJudul', 'Data Pembelian')

@section('isi-konten')
<div class="content">
    <div class="container-fluid">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary d-flex justify-content-between align-items-center">
                        <h4 class="card-title m-0">Data Pembelian</h4>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <tr>
                                    <th>ID</th>
                                    <th>Kode Buku</th>
                                    <th>Nama Pembeli</th>
                                    <th>Tanggal Pembelian</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pembelianList as $pembelian)
                                    <tr>
                                        <td>{{ $pembelian->id }}</td>
                                        <td>{{ $pembelian->kode_buku }}</td>
                                        <td>{{ $pembelian->nama_user }}</td>
                                        <td>{{ $pembelian->tanggal_pembelian }}</td>
                                        <td>{{ $pembelian->harga }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
