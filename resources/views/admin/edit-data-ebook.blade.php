@extends('sidebar.sidebar-editdata')

@section('Judul', 'E-Library | Edit Data E-Book')

@section('SubJudul', 'Edit Data E-book')

@section('isi-konten')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Edit Data E-book</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.edit-data-ebook.update', $book->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="kode_ebook">Kode E-Book</label>
                                    <input type="text" class="form-control" id="kode_ebook" name="kode_ebook" value="{{ $book->kode_ebook }}">
                                </div>
                                <div class="form-group">
                                    <label for="gambars">Gambar</label>
                                    <input type="file" class="form-control-file" id="gambars" name="gambars" value="{{ $book->ganbars }}">
                                </div>
                                <div class="form-group">
                                    <label for="juduls">Judul</label>
                                    <input type="text" class="form-control" id="juduls" name="juduls" value="{{ $book->juduls}}">
                                </div>
                                <div class="form-group">
                                    <label for="penulis">Penulis</label>
                                    <input type="text" class="form-control" id="penulis" name="penulis" value="{{ $book->penulis }}">
                                </div>
                                <div class="form-group">
                                    <label for="penerbits">Penerbit</label>
                                    <input type="text" class="form-control" id="penerbits" name="penerbits" value="{{ $book->penerbits }}">
                                </div>
                                <div class="form-group">
                                    <label for="stoks">Stok</label>
                                    <input type="number" class="form-control" id="stoks" name="stoks" value="{{ $book->stoks }}">
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input type="number" class="form-control" id="harga" name="harga" value="{{ $book->harga }}">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('admin.data-ebook') }}" class="btn btn-secondary">Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
