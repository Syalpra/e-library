@extends('sidebar.adminsidebar')

@section('Judul', 'E-Library | Tambah E-Library')

@section('SubJudul', 'Tambah Data E-Library')

@section('isi-konten')
    <div class="content">
        <div class="container-fluid">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Tambah Data E-Library</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.tambah-data-elibrary.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="kode_buku">Kode Buku</label>
                                    <input type="text" class="form-control" id="kode_buku" name="kode_buku" placeholder="Masukkan Kode E-book">
                                </div>
                                <div class="form-group">
                                    <label for="gambars">Gambar</label>
                                    <input type="file" class="form-control" id="gambars" name="gambars">
                                </div>
                                <div class="form-group">
                                    <label for="juduls">Judul</label>
                                    <input type="text" class="form-control" id="juduls" name="juduls" placeholder="Masukkan Judul">
                                </div>
                                <div class="form-group">
                                    <label for="penulis">Penulis</label>
                                    <input type="text" class="form-control" id="penulis" name="penulis" placeholder="Masukkan Penulis">
                                </div>
                                <div class="form-group">
                                    <label for="penerbits">Penerbit</label>
                                    <input type="text" class="form-control" id="penerbits" name="penerbits" placeholder="Masukkan Penerbit">
                                </div>
                                <div class="form-group">
                                    <label for="stoks">Stok</label>
                                    <input type="number" class="form-control" id="stoks" name="stoks" placeholder="Masukkan Stok">
                                </div>
                                <div class="form-group">
                                    <label for="raks">Rak</label>
                                    <input type="number" class="form-control" id="raks" name="raks" placeholder="Masukkan Harga">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
