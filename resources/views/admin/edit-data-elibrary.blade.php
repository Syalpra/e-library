@extends('sidebar.sidebar-editdata')

@section('Judul', 'E-Library | Edit Data E-Library')

@section('SubJudul', 'Edit Data E-Library')

@section('isi-konten')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Edit Data E-Library</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.edit-data-elibrary.update', $library->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="kode_buku">Kode Buku</label>
                                    <input type="text" class="form-control" id="kode_buku" name="kode_buku" value="{{ $library->kode_buku }}">
                                </div>
                                <div class="form-group">
                                    <label for="gambars">Gambar</label>
                                    <input type="file" class="form-control-file" id="gambars" name="gambars" value="{{ $library->gambars }}">
                                </div>
                                <div class="form-group">
                                    <label for="juduls">Judul</label>
                                    <input type="text" class="form-control" id="juduls" name="juduls" value="{{ $library->juduls}}">
                                </div>
                                <div class="form-group">
                                    <label for="penulis">Penulis</label>
                                    <input type="text" class="form-control" id="penulis" name="penulis" value="{{ $library->penulis }}">
                                </div>
                                <div class="form-group">
                                    <label for="penerbits">Penerbit</label>
                                    <input type="text" class="form-control" id="penerbits" name="penerbits" value="{{ $library->penerbits }}">
                                </div>
                                <div class="form-group">
                                    <label for="stoks">Stok</label>
                                    <input type="number" class="form-control" id="stoks" name="stoks" value="{{ $library->stoks }}">
                                </div>
                                <div class="form-group">
                                    <label for="raks">Raks</label>
                                    <input type="number" class="form-control" id="raks" name="raks" value="{{ $library->raks }}">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('admin.data-elibrary') }}" class="btn btn-secondary">Batal</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
