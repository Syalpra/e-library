<?php

use Illuminate\Support\Facades\Route;

// Untuk Login
use App\Http\Controllers\AuthController;

// Untuk Register
use App\Http\Controllers\RegisterController;

//untuk admin
use App\Http\Controllers\BookController;
use App\Http\Controllers\LibraryController;
use App\Models\Library;
use App\Models\Book;

//untuk member
use App\Http\Controllers\BookMemberController;
use App\Http\Controllers\LibraryMemberController;
use App\Models\LibraryMember;
use App\Models\BookMember;

use App\Http\Controllers\DataPinjamanController;

use App\Http\Controllers\PinjamanController;
use App\Http\Controllers\PembelianController;

use App\Http\Controllers\DataMemberController;



Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);


Route::get('/', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('/', [AuthController::class, 'login']);
Route::post('/admin/logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware('auth:admin')->group(function () {

    Route::get('/admin', [DataMemberController::class, 'index'])->name('admin.dashboard');

    Route::get('/admin/data-users', [DataMemberController::class, 'index2'])->name('admin.data-users');

    Route::get('admin/tambah-data-ebook', [BookController::class, 'create'])->name('admin.tambah-data-ebook.create');
    Route::post('admin/tambah-data-ebook', [BookController::class, 'store'])->name('admin.tambah-data-ebook.store');
    
    Route::get('/admin/edit-data-ebook/{id}', [BookController::class, 'edit'])->name('admin.edit-data-ebook.edit');
    Route::put('/admin/edit-data-ebook/{id}', [BookController::class, 'update'])->name('admin.edit-data-ebook.update');   

    // Menambah dan edit data library
    
    Route::get('admin/tambah-data-elibrary', [LibraryController::class, 'create'])->name('admin.tambah-data-elibrary.create');
    Route::post('admin/tambah-data-elibrary', [LibraryController::class, 'store'])->name('admin.tambah-data-elibrary.store');
    
    Route::get('/admin/edit-data-elibrary/{id}', [LibraryController::class, 'edit'])->name('admin.edit-data-elibrary.edit');
    Route::put('/admin/edit-data-elibrary/{id}', [LibraryController::class, 'update'])->name('admin.edit-data-elibrary.update');   


    // Menghapus data ebook ke dalam database
    Route::get('/admin/data-ebook', [BookController::class, 'index'])->name('admin.data-ebook');
    Route::delete('/admin/hapus-data-ebook/{id}', [BookController::class, 'destroy'])->name('ebook.destroy');
    
    //Menghapus data library
    Route::get('/admin/data-elibrary', [LibraryController::class, 'index'])->name('admin.data-elibrary');
    Route::delete('/admin/hapus-data-library/{id}', [LibraryController::class, 'destroy'])->name('library.destroy');
    
    
    Route::get('/admin/data-pembelian', [PembelianController::class, 'index2'])->name('admin.data-pembelian');
    Route::get('/admin/data-pinjaman', [DataPinjamanController::class, 'index2'])->name('admin.data-pinjaman');
});



Route::middleware('auth:member')->group(function () {
    Route::get('/member', function () {
        return view('member.dashboard-member');
    })->name('member.dashboard-member');
    Route::get('/member/ebook', [BookMemberController::class, 'index'])->name('member.ebook');
    Route::post('/member/beli-ebook/{id}', [BookMemberController::class, 'beliEbook'])->name('member.beli-ebook');
    Route::get('/member/pembelian', [PembelianController::class, 'index'])->name('member.pembelian');
    Route::get('/member/elibrary', [LibraryMemberController::class, 'index']);


    Route::get('/member/data-pinjaman', [DataPinjamanController::class, 'index'])->name('member.data-pinjaman');
    Route::post('/member/bayar-denda/{id}', [DataPinjamanController::class, 'bayarDenda'])->name('member.bayar-denda');
    Route::post('/member/buku-dikembalikan/{id}', [DataPinjamanController::class, 'bukuDikembalikan'])->name('member.buku-dikembalikan');
   

    Route::post('/pinjam-buku/{id}', [LibraryMemberController::class, 'pinjamBuku'])->name('pinjam.buku');
    Route::post('member/beli-ebook/{id}', [BookMemberController::class, 'beliEbook'])->name('member.beli-ebook');
});





