<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_librarys', function (Blueprint $table) {
            $table->id();
            $table->string('gambars');
            $table->string('juduls');
            $table->string('penerbits');
            $table->string('penulis');
            $table->string('raks');
            $table->integer('stoks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_librarys');
    }
};
